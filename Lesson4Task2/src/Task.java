import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;

/**
 * Created by helldes on 14.11.2014.
 */
public class Task {
    private String category;
    private String description;

    public String getCategory(){
        return this.category;
    }

    public String getDescription(){
        return this.description;
    }

    public void setCategory(String category){
        this.category = category;
    }

    public void setDescription(String description){
        this.description = description;
    }
}
