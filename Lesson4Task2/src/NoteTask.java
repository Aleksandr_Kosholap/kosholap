import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by helldes on 17.11.2014.
 */
public class NoteTask extends Operation {
        List<DayTasks> noteTask = new LinkedList<DayTasks>();

        public void addTask(Date date, Task task){
            boolean notDate = true;
            for(DayTasks o : noteTask){
                if(date.equals(o.date)){
                    o.task.add(task);
                    notDate = false;
                }
            }
            if (notDate){
                if (noteTask.size()== 0){
                    noteTask.add(new DayTasks(date, task));
                }else{
                    int size = noteTask.size();
                    for (int i = 0; i < size; i++){
                        DayTasks dateTask = noteTask.get(i);
                        Date newDate = dateTask.date;
                        int temp = newDate.compareTo(date);
                        if (temp == 1){
                            noteTask.add(i, new DayTasks(date, task));
                            break;
                        }
                        if(i == size-1){
                            noteTask.add(new DayTasks(date, task));
                        }
                    }
                }
            }
        }

    public void removeTask(Date date){
        boolean temp=false;
        DayTasks tempDayTask = null;
        for(DayTasks o : noteTask){
            if(date.equals(o.date)){
                temp = true;
                tempDayTask = o;
            }
        }
        if(temp){
            noteTask.remove(tempDayTask);
        }else {
            System.out.println("Not Date");
        }
    }

    public Collection<String> getCategories(){
        Set set = new LinkedHashSet();
        for (DayTasks o : noteTask){
            Task task = (Task) o.task;
            set.add(task.getCategory());
        }
        return (Collection) set;
    }

    public List<Task> getTasksByCategory(String category){
        Set set = new LinkedHashSet();
        for (DayTasks o : noteTask){
            List<Task> listTask =  o.task;
            for (Task u : listTask){
                if ( u.getCategory().equals(category)){
                    set.add(u);
                }
            }
        }
        List list = new LinkedList();
        list.addAll(set);
        return list;
    }

    public List<Task> getTasksForToday() throws ParseException {
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.mm.yyyy");
        Date today = formatDate.parse(String.valueOf(new Date()));
        LinkedList linkedList = new LinkedList();
        for(DayTasks o : noteTask){
            Task task = (Task) o.task;
            if(today.equals(o.date)){
                linkedList.add(task);
            }
        }
        return linkedList;
    }

    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> map = new HashMap();
        LinkedList category = (LinkedList) getCategories();
        for (Object o : category){
            map.put(o.toString(),getTasksByCategory(o.toString()));
        }
        return map;
    }

}
