import java.text.ParseException;
import java.util.*;

/**
 * Created by helldes on 15.11.2014.
 */
public abstract class Operation implements TaskManager {
    @Override
    public void addTask(Date date, Task task) {

    }

    @Override
    public void removeTask(Date date) {

    }

    @Override
    public Collection<String> getCategories() {
        return null;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        return null;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        return null;
    }

    @Override
    public List<Task> getTasksForToday() throws ParseException {
        return null;
    }
}
