import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by helldes on 14.11.2014.
 */
public class Lesson4Task2 extends Operation{
    public static void main(String[] args) {
        NoteTask myNoteTask = new NoteTask();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd.mm.yyyy");
        Date date = null;
        Task task;
        boolean temp = true;
        do {
            System.out.println(" 'c' - create new Task, 'd' - delete Task, 'i' - receiving collection method getCategories," +
                    " 'o' -receiving List<Task> method getTaskByCategory, 't' - get Task for Today, 'm' -receiving Map method getTaskByCategories");
            Scanner inString = new Scanner(System.in);
            String choice = inString.next();
            switch (choice.charAt(0)) {
                case 'c': {
                    task = new Task();
                    System.out.println("Create new Task:");
                    System.out.println("Input category for new Task:");
                    inString = new Scanner(System.in);
                    task.setCategory(inString.nextLine());
                    System.out.println("Input description for new Task:");
                    task.setDescription(inString.nextLine());
                    date = null;
                    System.out.println("Input Date(dd.mm.yyyy) for new Task:");
                    try {
                        date = formatDate.parse(inString.nextLine());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    myNoteTask.addTask(date,task);
                    break;
                }
                case 'd': {
                    System.out.println("Input Date(dd.mm.yyyy) for new Task:");
                    date = null;
                    inString = new Scanner(System.in);
                    try {
                        date = formatDate.parse(inString.nextLine());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    myNoteTask.removeTask(date);
                    break;
                }
                case 'i':{
                    Collection<String> collection = myNoteTask.getCategories();
                    System.out.println(collection.toString());
                    break;
                }
                case 'o':{
                    System.out.println("Enter the name of the category:");
                    inString = new Scanner(System.in);
                    List<Task> newList = myNoteTask.getTasksByCategory(inString.nextLine());
                    for (Task o : newList){
                        System.out.println("Category : " + o.getCategory() + " , description :" + o.getDescription());
                    }
                    break;
                }
                case 't':{
                    List<Task> newList = null;
                    try {
                        newList = myNoteTask.getTasksForToday();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    for (Task o : newList){
                        System.out.println("Category : " + o.getCategory() + " , description :" + o.getDescription());
                    }
                    break;
                }
                case 'm' : {
                    Map<String, List<Task>> map = myNoteTask.getTasksByCategories();
                    System.out.println(map.toString());
                    break;
                }
            }
        }while (temp) ;

    }
}