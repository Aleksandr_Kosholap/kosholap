import java.util.Scanner;

public class fibanachi {

    public static int recursionFibonachi(int number) {
        if (number <= 1) {
            return number;
        } else {
            return recursionFibonachi(number - 2) + recursionFibonachi(number - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите n: ");
        Scanner inString = new Scanner(System.in);
        if (inString.hasNextInt()) {
            int number = inString.nextInt();
            if (number > 0) {
                System.out.print("Последовательность полученная рекурсией: ");
                for (int i = 0; i < number; i++) {
                    System.out.print(recursionFibonachi(i) + " ");
                }
            } else {
                System.out.println("Число должно быть больше 0! ");
            }
        } else {
            System.out.print("Данные не коректные!");
        }
    }
}
