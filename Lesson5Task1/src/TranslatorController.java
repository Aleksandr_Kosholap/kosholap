import source.MyException;
import source.SourceLoader;
import source.URLSourceProvider;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());
        System.out.println("Enter 'exit' to exit the program");
        System.out.println("Enter the path to the file :");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //TODO: add exception handling here to let user know about it and ask him to enter another path to translation
            //      So, the only way to stop the application is to do that manually or type "exit"
            String source = null;
            try {
                source = sourceLoader.loadSource(command);

                File fileSource = new File("Source.txt");
                FileWriter fileWriterSourse = new FileWriter(fileSource);
                fileWriterSourse.append(source);
                fileWriterSourse.flush();
                fileWriterSourse.close();

                String translation = translator.translate(source);

                File fileTranslate = new File("Translation.txt");
                FileWriter fileWriterTranslate = new FileWriter(fileTranslate);
                fileWriterTranslate.append(translation);
                fileWriterTranslate.flush();
                fileWriterTranslate.close();
                System.out.println("Original: " + source);
                System.out.println();
                System.out.println("Translation: " + translation);
            } catch (MyException e) {
                System.err.println(e);
            }
            System.out.println("Enter the path to the file :");
            command = scanner.next();
        }
    }
}

