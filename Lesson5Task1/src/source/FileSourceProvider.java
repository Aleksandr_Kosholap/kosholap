package source;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        try {
            FileInputStream ifStream = new FileInputStream(pathToSource);
        } catch (FileNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder fileContent = new StringBuilder();
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader( new FileInputStream(pathToSource)))){
            String str = "";
            while( (str = buffer.readLine() ) != null ){
                fileContent.append(str);
            }
        }catch (IOException e){

        }
        return fileContent.toString();
    }
}
