package source;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        try {
            new URL(pathToSource).openStream();
        } catch (IOException e) {
           // e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        URL url = new URL(pathToSource);
        String fileContent = "";
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream()))){
            String str = null;
            while( (str = buffer.readLine() ) != null ){
                fileContent = fileContent +str;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return fileContent;
    }
}
