package source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        //TODO: initialize me
        sourceProviders.add(new FileSourceProvider());
        sourceProviders.add(new URLSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException, MyException {
        //TODO: implement me
        String content = "";
        for (int i = 0; i < sourceProviders.size(); i++) {
            if (sourceProviders.get(i).isAllowed(pathToSource)) {
                content = sourceProviders.get(i).load(pathToSource);
                if (content.equals("")) {
                    throw new MyException("No Content");
                }
                return content;
            }
        }
        if (!((sourceProviders.get(0).isAllowed(pathToSource)) && (sourceProviders.get(1).isAllowed(pathToSource)))){
            throw new MyException("No Source");
        }

        return content;
    }
}
