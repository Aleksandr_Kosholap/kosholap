package json.adapters;

import json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        //implement me
        JSONArray m = new JSONArray();
        for (Object o : c) {
            m.put(o);
        }
        return m;
    }
}
