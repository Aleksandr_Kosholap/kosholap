/**
 * Created by helldes on 03.11.2014.
 */
public class Main {

    public static void main(String[] args){
        String testString = "";
        String addString = "TEST";
        int sizeLoop = 200000;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i <= sizeLoop; i++){
            testString = testString + addString;
        }
        long time = System.currentTimeMillis() - startTime;
        System.out.println("Runtime concatenation String = " + time);

        StringBuffer testStringBuffer = new StringBuffer();
        startTime = System.currentTimeMillis();
        for (int i = 0; i <= sizeLoop; i++){
            testStringBuffer.append(addString);
        }
        time = System.currentTimeMillis() - startTime;
        System.out.println("Runtime concatenation StringBuffer = " + time);

        StringBuilder testStringBuilder = new StringBuilder();
        startTime = System.currentTimeMillis();
        for (int i = 0; i <= sizeLoop; i++){
            testStringBuilder.append(addString);
        }
        time = System.currentTimeMillis() - startTime;
        System.out.println("Runtime concatenation StringBuilder = " + time);
    }

}
