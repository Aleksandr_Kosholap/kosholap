public interface Driveable {
    public boolean stateEngine();

    public int stateFuels();

    public void fill() throws MyException;

    public void accelerate() throws MyException;

    public void brake() throws MyException;

    public void turn(int i) throws MyException;

    public String printDirection();
}
