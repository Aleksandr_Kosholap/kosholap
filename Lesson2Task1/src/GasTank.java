/**
 * Created by helldes on 02.11.2014.
 */
public class GasTank implements EnergeProvider {
    final int MAX_VOLUME;
    final int MIN_VOLUME = 0;
    private int fuels = 0;

    public GasTank(int volumeGasTank) {
        this.MAX_VOLUME = volumeGasTank;
    }


    public void useFuels() {
        this.setFuels(this.getFuels() - 1);
    }

    public int getFuels() {
        return this.fuels;
    }

    public void setFuels(int fuels) {
        this.fuels = fuels;
    }
}
