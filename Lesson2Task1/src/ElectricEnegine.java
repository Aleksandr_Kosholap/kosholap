/**
 * Created by helldes on 02.11.2014.
 */
public class ElectricEnegine implements ForceProvider {
    boolean stateEngine;
    @Override
    public void startEngine() {
        setStateEngine(true);
    }

    @Override
    public void stopEngine() {
        setStateEngine(false);
    }
    public boolean getStateEngine() {
        return this.stateEngine;
    }

    private void setStateEngine(boolean i) {
        this.stateEngine = i;
    }
}
