import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws MyException {

        Car myVehicle = new Car(60, 100, 8);
        //Boat myVehicle = new Boat();
        boolean temp = true;
        do {
            Scanner inString = new Scanner(System.in);
            String choice = inString.next();
            switch (choice.charAt(0)) {
                case 'R': {
                    myVehicle.turn(1);
                    break;
                }
                case 'L': {
                    myVehicle.turn(-1);
                    break;
                }
                case 'A': {
                    myVehicle.accelerate();
                    break;
                }
                case 'F': {
                    try {
                        myVehicle.fill();
                    } catch (MyException e) {
                        System.err.println(e);
                    }
                    break;
                }
                case 'B': {
                    try {
                        myVehicle.brake();
                    } catch (MyException e) {
                        System.err.println(e);
                    }
                    break;
                }
                case 'S': {
                    myVehicle.startEngine();
                    break;
                }
                case 'Q': {
                    temp = false;
                    MyException.printCountExeption();
                    break;
                }
                case '?': {
                    System.out.println("Управление транспортом: R-поворот на право, L-поворот на лево, A-ускорение, F-заправится, B-тормозить, " +
                            "S-запустить двигатель, Q-выход");
                    break;
                }
            }
            System.out.println("Статус машины: топлива - " + myVehicle.stateFuels() + ", скорость - " + myVehicle.getSpeed() +
                    ", наше направление - " + myVehicle.printDirection()+ ", cостояние двигателя " + myVehicle.stateEngine());
        } while (temp);
    }
}
