import java.util.Scanner;

public class Boat extends Vehicle{
    private int volumeMaxSpeed;
    private int volumeGasTank;
    private int expenditure;
    public Boat(int volumeGasTank, int volumeMaxSpeed, int expenditureFuel){
        this.volumeGasTank = volumeGasTank;
        this.volumeMaxSpeed = volumeMaxSpeed;
        this.expenditure = expenditureFuel;
    }
    Propeller propeller = new Propeller();
    DieselEngine dieselEngine = new DieselEngine();
    GasTank gasTank = new GasTank(this.volumeGasTank);


    public boolean stateEngine() {
        return dieselEngine.getStateEngine();
    }

    public int stateFuels() {
        return gasTank.getFuels();
    }

    public void fill() throws MyException{
        gasTank= new GasTank(this.volumeGasTank);
        System.out.println("Сколько литров заправить?");
        Scanner inString = new Scanner(System.in);
        if (inString.hasNextInt()) {
            int number = inString.nextInt();
            if ((number > gasTank.MIN_VOLUME) && (number < gasTank.MAX_VOLUME)) {
                if ((number + gasTank.getFuels()) <= gasTank.MAX_VOLUME) {
                    gasTank.setFuels(number + gasTank.getFuels());
                } else {
                    throw new MyException("Ещё можно залить всего: " + (gasTank.MAX_VOLUME - gasTank.getFuels()) + "литров");
                }
            } else {
                throw new MyException ("Бензин не сливаем и не трамбуем! заливаем от " + (gasTank.MIN_VOLUME + 1) + " до " +
                        gasTank.MAX_VOLUME + " литров");
            }
        }
    }

    @Override
    public void accelerate() {
        if (dieselEngine.getStateEngine()) {
            if (getSpeed() < this.volumeMaxSpeed) {
                if (gasTank.getFuels() > 0) {
                    gasTank.useFuels();
                    setSpeed(getSpeed() + 10);
                } else {
                    dieselEngine.stopEngine();
                    System.out.println("Двигатель заглох! Нет топлива!");
                    setSpeed(0);
                }

            } else {
                System.out.println("Двигатель закипит!");
            }
        } else {
            System.out.println("Двигатель выключен!");
        }
    }

    @Override
    public void brake() {
        if (dieselEngine.getStateEngine()) {
            if (getSpeed() > 0) {
                dieselEngine.stopEngine();
                System.out.println("Якорь отсутствует, глушу двигатель");
                this.setSpeed(0);
            } else {
                System.out.println("Плывём по течению");
            }
        } else {
            System.out.println("Двигатель выключен! Стоим на месте");
        }

    }

    @Override
    public void turn(int i) {
            if (i == 1) {
                propeller.turnRight();
            } else {
                propeller.turnLeft();
            }
        }

    public void startEngine() {
        if (dieselEngine.getStateEngine()) {
            dieselEngine.stopEngine();
            setSpeed(0);
        }else{
            if (gasTank.getFuels() > 0) {
                dieselEngine.startEngine();
            } else {
                System.out.println("Не стартует, наверное Бак пустой!");
            }
        }
    }
    public String printDirection(){
        if (propeller.getDirectionPropeller() == -1){
            return "LEFT";
        }else {
            if (propeller.getDirectionPropeller()== 0){
                return "CENTER";
            } else {
                return "RIGTH";
            }
        }
    }
}
