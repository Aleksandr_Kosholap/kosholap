/**
 * Created by helldes on 02.11.2014.
 */
public class SolarBattery implements EnergeProvider {
    public final int MAX_VOLUME = 100;
    public final int MIN_VOLUME = 0;
    private int energe = 0;



    public void useFuels() {
        this.setFuels(this.getFuels() - 1);
    }

    public int getFuels() {
        return this.energe;
    }

    public void setFuels(int energe) {
        this.energe = energe;
    }
}
