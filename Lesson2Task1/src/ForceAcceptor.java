/**
 * Created by helldes on 31.10.2014.
 */
public interface ForceAcceptor {
    public void turnRight();
    public void turnLeft();
}
