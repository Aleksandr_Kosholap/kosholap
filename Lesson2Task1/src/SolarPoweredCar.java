import java.util.Scanner;

/**
 * Created by helldes on 02.11.2014.
 */
public class SolarPoweredCar extends Vehicle {

    private int volumeMaxSpeed;
    private int expenditure;

    public SolarPoweredCar( int volumeMaxSpeed, int expenditureFuel){
        this.volumeMaxSpeed = volumeMaxSpeed;
        this.expenditure = expenditureFuel;
    }

    Wheels wheels = new Wheels();
    ElectricEnegine electricEnegine = new ElectricEnegine();
    SolarBattery solarBattery = new SolarBattery();

    @Override
    public boolean stateEngine() {
        return electricEnegine.getStateEngine();
    }

    @Override
    public int stateFuels() {
        return solarBattery.getFuels();
    }

    @Override
    public void fill() throws MyException {
        solarBattery= new SolarBattery();
        System.out.println("Сколько % зарядить?");
        Scanner inString = new Scanner(System.in);
        if (inString.hasNextInt()) {
            int number = inString.nextInt();
            if ((number > solarBattery.MIN_VOLUME) && (number < solarBattery.MAX_VOLUME)) {
                if ((number + solarBattery.getFuels()) <= solarBattery.MAX_VOLUME) {
                    solarBattery.setFuels(number + solarBattery.getFuels());
                } else {
                    throw new MyException("Ещё можно зарядить всего: " + (solarBattery.MAX_VOLUME - solarBattery.getFuels()) + "литров");
                }
            } else {
                throw new MyException ("Всего можно зарядить от " + (solarBattery.MIN_VOLUME + 1) + "% до " +
                        solarBattery.MAX_VOLUME + "% енергии");
            }
        }
    }

    @Override
    public void accelerate() throws MyException {
        if (electricEnegine.getStateEngine()) {
            if (getSpeed() < 100) {
                if (solarBattery.getFuels() > 0) {
                    solarBattery.useFuels();
                    setSpeed(getSpeed() + 10);
                } else {
                    electricEnegine.stopEngine();
                    setSpeed(0);
                    throw new MyException("Двигатель выключен! Нет энергии!");
                }

            } else {
                throw new MyException("Двигатель на максимальной мощьности!");
            }
        } else {
            throw new MyException("Двигатель выключен!");
        }

    }

    @Override
    public void brake() throws MyException {
        if (electricEnegine.getStateEngine()) {
            if (getSpeed() > 0) {
                setSpeed(getSpeed() - 10);
            } else {
                throw new MyException("Тормозить незачем! и так стоим!");
            }
        } else {
            throw new MyException("Двигатель выключен! Стоим на месте");
        }

    }

    @Override
    public void turn(int i) throws MyException {
        if (getSpeed() > 0) {
            if (i == 1) {
                wheels.turnRight();
            } else {
                wheels.turnLeft();
            }
        } else {
            throw new MyException("Стоим на месте, начните сперва движение");
        }

    }

    @Override
    public String printDirection() {
        if (wheels.getDirectionWheels() == -1) {
            return "LEFT";
        } else {
            if (wheels.getDirectionWheels() == 1) {
                return "RIGTH";
            } else {
                return "CENTER";
            }
        }
    }
}
