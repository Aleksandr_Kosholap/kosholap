import java.util.Scanner;

public class Car extends Vehicle {

    private int volumeMaxSpeed;
    public int volumeGasTank;
    private int expenditure;

    public Car(int maxVolumeGasTank, int volumeMaxSpeed, int expenditureFuel){
        this.volumeGasTank = maxVolumeGasTank;
        this.volumeMaxSpeed = volumeMaxSpeed;
        this.expenditure = expenditureFuel;
    }

    Wheels wheels = new Wheels();
    DieselEngine dieselEngine = new DieselEngine();
    GasTank gasTank;

    @Override
    public boolean stateEngine() {
        return dieselEngine.getStateEngine();
    }

    @Override
    public int stateFuels() {
        return gasTank.getFuels();
    }

    public void fill() throws MyException{
        gasTank= new GasTank(this.volumeGasTank);
        System.out.println("Сколько литров заправить?");
        Scanner inString = new Scanner(System.in);
        if (inString.hasNextInt()) {
            int number = inString.nextInt();
            if ((number > gasTank.MIN_VOLUME) && (number < gasTank.MAX_VOLUME)) {
                if ((number + gasTank.getFuels()) <= gasTank.MAX_VOLUME) {
                    gasTank.setFuels(number + gasTank.getFuels());
                } else {
                    throw new MyException("Ещё можно залить всего: " + (gasTank.MAX_VOLUME - gasTank.getFuels()) + "литров");
                }
            } else {
                throw new MyException ("Бензин не сливаем и не трамбуем! заливаем от " + (gasTank.MIN_VOLUME + 1) + " до " +
                        gasTank.MAX_VOLUME + " литров");
            }
        }
    }

    public void brake() throws MyException{
        if (dieselEngine.getStateEngine()) {
            if (getSpeed() > 0) {
                setSpeed(getSpeed() - 10);
            } else {
                throw new MyException("Тормозить незачем! и так стоим!");
            }
        } else {
            throw new MyException("Двигатель выключен! Стоим на месте");
        }

    }

    public void turn(int i) throws MyException{
        if (getSpeed() > 0) {
            if (i == 1) {
                wheels.turnRight();
            } else {
                wheels.turnLeft();
            }
        } else {
            throw new MyException("Стоим на месте, начните сперва движение");
        }
    }

    @Override
    public String printDirection() {
        if (wheels.getDirectionWheels() == -1) {
            return "LEFT";
        } else {
            if (wheels.getDirectionWheels() == 1) {
                return "RIGTH";
            } else {
                return "CENTER";
            }
        }
    }

    public void startEngine() throws MyException{
        if (dieselEngine.getStateEngine()) {
            dieselEngine.stopEngine();
            setSpeed(0);
        }else{
            if (gasTank.getFuels() > 0) {
                dieselEngine.startEngine();
            } else {
                throw new MyException("Не стартует, наверное Бак пустой!");
            }
        }
    }

    public void accelerate() throws MyException{
        if (dieselEngine.getStateEngine()) {
            if (getSpeed() < 100) {
                if (gasTank.getFuels() > 0) {
                    gasTank.useFuels();
                    setSpeed(getSpeed() + 10);
                } else {
                    dieselEngine.stopEngine();
                    setSpeed(0);
                    throw new MyException("Двигатель заглох! Нет топлива!");
                }

            } else {
                throw new MyException("Двигатель закипит!");
            }
        } else {
            throw new MyException("Двигатель выключен!");
        }
    }

}

