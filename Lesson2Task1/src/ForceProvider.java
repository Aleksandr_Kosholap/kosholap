/**
 * Created by helldes on 02.11.2014.
 */
public interface ForceProvider {
    public void startEngine();
    public void stopEngine();
}
