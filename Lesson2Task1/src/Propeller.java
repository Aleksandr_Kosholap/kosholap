public class Propeller implements ForceAcceptor{
    int directionPropeller = 0;

    public void turnRight() {
        if (getDirectionPropeller() != 1) {
            setDirectionPropeller(getDirectionPropeller() + 1);
        } else {
            System.out.println("Пропеллер повёрнут на право");
        }
    }

    public void turnLeft() {
        if (getDirectionPropeller() != -1) {
            setDirectionPropeller(getDirectionPropeller() - 1);
        } else {
            System.out.println("Пропеллер повёрнут на лево");
        }
    }

    private void setDirectionPropeller(int change) {
        this.directionPropeller = change;
    }

    public int getDirectionPropeller() {
        return this.directionPropeller;
    }
}
