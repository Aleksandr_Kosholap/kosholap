class DieselEngine implements ForceProvider{
    boolean stateEngine;


    public boolean getStateEngine() {
        return this.stateEngine;
    }

    private void setStateEngine(boolean i) {
        this.stateEngine = i;
    }

    @Override
    public void startEngine() {
        setStateEngine(true);
    }

    @Override
    public void stopEngine() {
        setStateEngine(false);
    }
}
