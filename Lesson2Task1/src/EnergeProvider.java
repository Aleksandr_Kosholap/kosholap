/**
 * Created by helldes on 02.11.2014.
 */
interface EnergeProvider {
    public int getFuels();
    public void setFuels(int fuels);
    public void useFuels();


}
