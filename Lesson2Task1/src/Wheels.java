class Wheels implements ForceAcceptor{
    int directionWheels = 0;

    public void turnRight() {
        if (getDirectionWheels() != 1) {
            setDirectionWheels(getDirectionWheels() + 1);
        } else {
            System.out.println("Колёса повёрнуты на право");
        }
    }

    public void turnLeft() {
        if (getDirectionWheels() != -1) {
            setDirectionWheels(getDirectionWheels() - 1);
        } else {
            System.out.println("Колёса повёрнуты на лево");
        }
    }

    private void setDirectionWheels(int change) {
        this.directionWheels = change;
    }

    public int getDirectionWheels() {
        return this.directionWheels;
    }
}
