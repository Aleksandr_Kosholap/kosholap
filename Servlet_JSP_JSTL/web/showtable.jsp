<%--
  Created by IntelliJ IDEA.
  User: helldes
  Date: 29.01.2015
  Time: 12:28
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<form name="table" action="/addentry" method="post">
    <table border="1" width="40%">
        <tr bgcolor="#5f9ea0">
            <td width="45%">Name:</td>
            <td width="45%">Value:</td>
            <td width="10%">Action</td>
        </tr>

        <c:forEach var="res" items="${resultObjects}">
            <tr>
            <c:set var="showtag1" value="true"/>
            <c:set var="showtag2" value="false"/>
            <c:if test="${showtag1}">
                <c:set var="showtag1" value="false"/>
                <td><c:out value="${res.name}"/></td>
            </c:if>
            <c:forEach var="res1" items="${res.valueNames}">
                <c:if test="${showtag1}">
                    <tr>
                    <td></td>
                    <c:set var="refreshSent" value="true"/>
                </c:if>
                <c:if test="${showtag2}">
                    <td></td>
                </c:if>
                <c:set var="showtag2" value="true"/>
                <td><c:out value="${res1.valueNames}"/></td>
                <td><a href="/delete?id=${res1.id}"><c:out value="Delete"/> </a></td>
                </tr>
            </c:forEach>

            </tr>
        </c:forEach>
        <tr>
            <td><input type="text" name="name" size="20"></td>
            <td><input type="text" name="valueNames" size="20"></td>
            <td><input type="submit" value="Add"></td>
        </tr>
    </table>
</form>
</body>
</html>
