import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by helldes on 30.01.2015.
 */
public class CreateObjects {

    public List<GroupName> createObject(ResultSet resultSet) throws SQLException {
        List<GroupName> entitys = new LinkedList<>();
        while (resultSet.next()) {
            Boolean testNewEntity = true;
            GroupName entity = new GroupName();

            ValueNames valueNames = new ValueNames();
            valueNames.setId(resultSet.getInt("id"));
            valueNames.setName(resultSet.getString("name"));
            valueNames.setValueNames(resultSet.getString("valueName"));

            entity.setValueNames(valueNames);
            entity.setName(resultSet.getString("name"));
            for (GroupName groupName : entitys){
                if (groupName.getName().equals(entity.getName())){
                    groupName.groupValues.add(valueNames);
                    testNewEntity = false;
                }
            }
            if (testNewEntity) {
                entitys.add(entity);
            }
        }
    return  entitys;
    }
}
