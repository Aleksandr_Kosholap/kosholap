/**
 * Created by helldes on 30.01.2015.
 */
public class ValueNames {
    public String name;
    public String valueNames;
    public int id;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValueNames(String valueNames) {
        this.valueNames = valueNames;
    }

    public String getValueNames() {
        return this.valueNames;
    }

    public int getId() {
        return id;
    }
}
