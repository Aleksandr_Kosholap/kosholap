import com.sun.rowset.internal.Row;

import java.sql.*;
import java.util.Objects;

/**
 * Created by helldes on 30.01.2015.
 */
public class Select {

    ResultSet rs = null;
    public ResultSet  selectdb(Connection connection) throws SQLException {
        String sql = "SELECT * FROM animals";
        Statement statement = connection.createStatement();
        rs = statement.executeQuery(sql);

        return rs;
    }
}
