import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by helldes on 30.01.2015.
 */
public class GroupName {
    String name;
    List<ValueNames> groupValues = new LinkedList<ValueNames>();

    public void setName(String name){
        this.name = name;
    }

    public void setValueNames(ValueNames valueNames) {
        this.groupValues.add(valueNames);
    }

    public String getName() {
        return this.name;
    }

    public List<ValueNames> getValueNames() {
        return this.groupValues;
    }
}
