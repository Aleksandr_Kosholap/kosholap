
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.List;

/**
 * Created by helldes on 29.01.2015.
 */
@WebServlet(name = "AddEntryServlet", urlPatterns = {"/addentry"})
public class AddEntryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String valueNames = request.getParameter("valueNames");
        Connection connection = (Connection)request.getSession().getAttribute("connection");
        try {
          //  response.setContentType("text/html");
           // PrintWriter out = response.getWriter();
            String sql = "INSERT INTO animals ( name, valueName) VALUES (\"" + name +"\", \"" + valueNames + "\")";
            PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ResultSet resultSet = new Select().selectdb(connection);
            List resultObjects = new CreateObjects().createObject(resultSet);
            request.setAttribute("resultObjects", resultObjects);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("showtable.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
