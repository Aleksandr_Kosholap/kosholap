
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.List;

/**
 * Created by helldes on 29.01.2015.
 */
@WebServlet(name = "ConnectionToDbServlet", urlPatterns = {"/connected"})
public class ConnectionToDbServlet extends HttpServlet {
    public Connection connection;
    private String url = "jdbc:mysql://127.0.0.1:3306/";
    private String dbName = "animalsdb";
    private String name = "root";
    private String password = "qwerty";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initConnection();
        try {
            ResultSet resultSet = new Select().selectdb(connection);
            List resultObjects = new CreateObjects().createObject(resultSet);
            request.setAttribute("resultObjects", resultObjects);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getSession().setAttribute("connection", connection);
        request.getRequestDispatcher("showtable.jsp").forward(request, response);
    }

    public void initConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url + dbName, name, password);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
