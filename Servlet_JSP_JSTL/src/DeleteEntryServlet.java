import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by helldes on 30.01.2015.
 */
@WebServlet(name = "DeleteEntryServlet", urlPatterns = {"/delete"})
public class DeleteEntryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql = "DELETE FROM animals WHERE id = " + request.getParameter("id");
        Connection connection = (Connection)request.getSession().getAttribute("connection");
        int rowsAffected = 0;
        List resultObjects = null;
        try  {
            Statement statement = connection.createStatement();
            rowsAffected = statement.executeUpdate(sql);

            ResultSet resultSet = new Select().selectdb(connection);
            resultObjects = new CreateObjects().createObject(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("resultObjects", resultObjects);
        request.getRequestDispatcher("showtable.jsp").forward(request, response);
    }
}
