<%--
  Created by IntelliJ IDEA.
  User: helldes
  Date: 25.01.2015
  Time: 14:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Edit File</title>
</head>
<body>
<form action="/savefile" method="post">
  <table border="1" width="80%">
    <tr>
      <td width="30%"><a href="/opendirectory?folder=null">Back</a></td>
      <td width="30%"><input type="submit" value="Save"></td>
      <td>Enter file name:&emsp;<input type="text" name="name_file" size="70"></td>
    </tr>
    <tr>
      <td colspan="3"><textarea name="content" rows="20" cols="150">input text</textarea></td>
    </tr>
  </table>
</form>
</body>
</html>
