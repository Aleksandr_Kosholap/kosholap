<%--
  Created by IntelliJ IDEA.
  User: helldes
  Date: 23.01.2015
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Edit File</title>
</head>
<body>
<form action="/savefile" method="post">
<table border="1" width="80%">
  <tr>
    <td><a href="/opendirectory?folder=null">Back</a></td>
    <td><input type="submit" value="Save"></td>
    <td><%=(request.getSession().getAttribute("name_file"))%></td>
  </tr>
  <tr>
    <td colspan="3"><textarea name="content" rows="20" cols="150"><%=(request.getAttribute("content"))%></textarea></td>
  </tr>
</table>
</form>
</body>
</html>
