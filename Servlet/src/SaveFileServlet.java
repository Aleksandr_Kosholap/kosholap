import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by helldes on 24.01.2015.
 */
@WebServlet(name = "SaveFileServlet", urlPatterns = {"/savefile"})
public class SaveFileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String content = request.getParameter("content");
        String newFile = request.getParameter("name_file");
        String sessionFile = (String)request.getSession().getAttribute("name_file");
        File fileSource = null;
        if (newFile != null){
            fileSource = new File (request.getSession().getAttribute("current_directory") + "\\" + newFile);
            request.getSession().setAttribute("name_file", request.getSession().getAttribute("current_directory") +"\\" + newFile);
        }else {
            fileSource = new File(sessionFile);
        }
        FileWriter fileWriterSourse = new FileWriter(fileSource);
        fileWriterSourse.append(content);
        fileWriterSourse.flush();
        fileWriterSourse.close();
        request.setAttribute("content", content );
        request.getRequestDispatcher("openfile.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
