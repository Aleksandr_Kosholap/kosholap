import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by helldes on 24.01.2015.
 */
public class ReadFile {
    static public String load(String pathToSource) throws IOException {
        StringBuilder fileContent = new StringBuilder();
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader( new FileInputStream(pathToSource)))){
            String str = "";
            while( (str = buffer.readLine() ) != null ){
                fileContent.append(str);
            }
        }catch (IOException e){

        }
        return fileContent.toString();
    }
}
