import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

/**
 * Created by helldes on 23.01.2015.
 */
@WebServlet(name = "OpenDirectoryServlet", urlPatterns = {"/opendirectory"})
public class OpenDirectoryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        File currentDirectory = null;
        if (session.getAttribute("current_directory") == null) {
            currentDirectory = new File("/");
            session.setAttribute("current_directory", currentDirectory.getAbsolutePath());
        } else {
            if (request.getParameter("folder").equals("back")) {
                String tempPath = (String) session.getAttribute("current_directory");
                int indexLast = tempPath.lastIndexOf('\\');
                currentDirectory = new File(tempPath.substring(0, indexLast + 1));
            } else {
                if (request.getParameter("folder").equals("null")) {
                    currentDirectory = new File(session.getAttribute("current_directory").toString());
                } else {
                    currentDirectory = new File(request.getParameter("folder"));
                }
            }

        }
        session.setAttribute("current_directory", currentDirectory.getAbsolutePath());
        out.print("<html>\n" +
                "<head>\n" +
                "    <title>Open File</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<form action=\"createfile.jsp\">\n" +
                "<table border=\"1\" width=\"80%\">\n" +
                "  <tr>\n" +
                "    <td><input type=\"submit\" value=\"Create File\"></td>\n" +
                "    <td width=\"80%\"><b>Current folder:</b> "+ currentDirectory +"</td>\n" +
                "  </tr>\n" +
                "</table><p></p>");
        if (currentDirectory.isDirectory()) {
            out.println("<a href=\"/opendirectory?folder=back \"\"> \\ .. </a><br>");
            for (File myFile : currentDirectory.listFiles()) {
                if (myFile.isDirectory()) {
                    out.println("<img src=\"image\\Folders-Windows-Folder-icon.png\"> <a href=\"/opendirectory?folder=" + myFile.toString() + "\">" + myFile.getName() + "</a><br>");
                } else {
                    int dotPos = myFile.toString().lastIndexOf(".");
                    String ext = myFile.toString().substring(dotPos);
                    if (ext.equals(".txt")) {

                        out.println("&emsp;<a href=\"/openfile?param=" + myFile.toString() + "\">" + myFile.getName() + "</a><br>");
                    } else {
                        out.println("&emsp;   " + myFile.getName() + "<br>");
                    }
                }

            }
        }
        out.print("</form>\n" +
                "</body>\n" +
                "</html>");
    }
}
