import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by helldes on 24.01.2015.
 */
@WebServlet(name = "EditFileServlet", urlPatterns = {"/editfile"})
public class EditFileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String valueSubmitDelete = "";
        valueSubmitDelete = request.getParameter("delete");
        String fileName = (String) request.getSession().getAttribute("name_file");
        if (valueSubmitDelete.equals("Delete")) {
            File file = new File(fileName);
            file.delete();
            request.getRequestDispatcher("filedelete.jsp").forward(request, response);
        } else {
            String content = new ReadFile().load(fileName);
            request.setAttribute("content", content);
            request.getRequestDispatcher("editfile.jsp").forward(request, response);
        }
    }
}

