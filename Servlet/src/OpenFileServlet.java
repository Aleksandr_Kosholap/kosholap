import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

/**
 * Created by helldes on 23.01.2015.
 */
@WebServlet(name = "OpenFileServlet", urlPatterns = {"/openfile"})
public class OpenFileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.print("test1");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String fileName = null;
        if (request.getParameter("param")=="null"){
            fileName = (String)session.getAttribute("name_file");
        }else {
            fileName = request.getParameter("param");
        }
        String content = new ReadFile().load(fileName);

        request.setAttribute("content", content );
        session.setAttribute("name_file", fileName );
        request.getRequestDispatcher("openfile.jsp").forward(request, response);
    }

    }
