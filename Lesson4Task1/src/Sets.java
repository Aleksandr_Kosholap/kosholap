import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by helldes on 13.11.2014.
 */
public class Sets implements SetOperations {


    public static Set createSet() {
        boolean exit = true;
        LinkedHashSet newSet = new LinkedHashSet();
        System.out.println("Input new elements in Set:");
        do {
            Scanner inString = new Scanner(System.in);
            if (inString.hasNextInt()) {
                newSet.add(inString.nextInt());
            } else {
                exit = false;
            }
        } while (exit);
        return newSet;
    }

    public static void printSet(Set set) {
        for (Object o : set) {
            System.out.print(o + " ");
        }
    }

    @Override
    public boolean equals(Set a, Set b) {
        boolean temp = false;
        if (a.size() != b.size()) {
            return false;
        } else {
            for (Object o : a) {
                for (Object u : b) {
                    if (o.equals(u)) {
                        temp = true;
                    }
                }
                if (!temp) {
                    return false;
                }
                temp = false;
            }
        }
        return true;
    }

    @Override
    public Set union(Set a, Set b) {
        Set newSet = new LinkedHashSet();
        newSet.addAll(a);
        newSet.addAll(b);
        return newSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set newSet = new LinkedHashSet();
        newSet.addAll(a);
        for (Object o : b) {
            for (Object u : a) {
                if (o.equals(u)) {
                    newSet.remove(u);
                }
            }
        }
        return newSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set newSet = new LinkedHashSet();
        for (Object o : a) {
            for (Object u : b) {
                if (o.equals(u)) {
                    newSet.add(o);
                }
            }
        }
        return newSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set newSetA = subtract(a, b);
        Set newSetB = subtract(b, a);
        newSetA.addAll(newSetB);
        return newSetA;
    }


}
