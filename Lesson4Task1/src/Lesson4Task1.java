import java.util.Set;

/**
 * Created by helldes on 13.11.2014.
 */
public class Lesson4Task1 {
    public static void main(String[] args) {
        System.out.println("Enter any letter to complete the filling of the set");
        System.out.println("Create SET A:");
        Set setA = Sets.createSet();
        System.out.println("Create SET B:");
        Set setB = Sets.createSet();
        System.out.print("SET A: ");
        Sets.printSet(setA);
        System.out.println();
        System.out.print("SET B: ");
        Sets.printSet(setB);
        System.out.println();

        Sets operations = new Sets();
        System.out.println("SET A equals SET B = " + operations.equals(setA, setB));

        Set unionSet = operations.union(setA, setB);
        System.out.print("Union Set : ");
        Sets.printSet(unionSet);
        System.out.println();

        Set subtractSet = operations.subtract(setA, setB);
        System.out.print("Subtract Set : ");
        Sets.printSet(subtractSet);
        System.out.println();

        Set intersectSet = operations.intersect(setA, setB);
        System.out.print("Intersect Set : ");
        Sets.printSet(intersectSet);
        System.out.println();

        Set symmetricSubtract = operations.symmetricSubtract(setA, setB);
        System.out.print("Symmetric Subtract Set : ");
        Sets.printSet(symmetricSubtract);

    }


}

