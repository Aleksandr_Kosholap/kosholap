import java.util.Set;

/**
 * Created by helldes on 13.11.2014.
 */
public interface SetOperations {
    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);
}
