import java.util.Random;

/**
 * Created by helldes on 04.11.2014.
 */
public class TeaKettle implements Comparable {

    private String collorTeaKettle;
    private  int volumeTeaKettle;
    private  String typeTeaKettle;
    static String[] collor = {"RED", "BLUE", "GREEN", "BLACK"};
    static int[] volume = {2, 3, 4, 5};
    static String[] type = {"ELECTRIC", "ORDINARY"};

    public TeaKettle(String collor, int volume, String type) {
        this.collorTeaKettle = collor;
        this.volumeTeaKettle = volume;
        this.typeTeaKettle = type;
    }

    public static TeaKettle createTeaKettle(){
        Random random = new Random();
        String tempCollor = collor[random.nextInt(collor.length)];
        int tempVolume = volume[random.nextInt(volume.length)];
        String tempType = type[random.nextInt(type.length)];
        TeaKettle teaKettle = new TeaKettle(tempCollor, tempVolume, tempType);
        return teaKettle;
    }
    public String getCollor(){
        return this.collorTeaKettle;
    }
    public int getVolume(){
        return  this.volumeTeaKettle;
    }
    public String getType(){
        return this.typeTeaKettle;
    }
    public void setCollor(String collor){
        this.collorTeaKettle = collor;
    }
    public void setVolume(int volume){
        this.volumeTeaKettle= volume;
    }
    public void setType(String type){
        this.typeTeaKettle = type;
    }

    @Override
    public int compareTo(Object obj) {
        TeaKettle teaKattle = (TeaKettle) obj;
        int result = collorTeaKettle.compareTo(teaKattle.collorTeaKettle);
        if(result != 0) {
            return result;
        }

        result = volumeTeaKettle - teaKattle.volumeTeaKettle;
        if(result != 0) {
            return (int) result / Math.abs( result );
        }

        result = typeTeaKettle.compareTo(teaKattle.typeTeaKettle);
        if (result != 0) {
            return result;
        }
        return 0;
    }
}
