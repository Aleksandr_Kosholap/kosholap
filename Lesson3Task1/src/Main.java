import java.util.Scanner;

/**
 * Created by helldes on 04.11.2014.
 */
public class Main {
    public static void main(String[] args) {
        int number = 0;
        System.out.println("Enter the number: ");
        Scanner inString = new Scanner(System.in);
        TeaKettle[] teaKettle = new TeaKettle[100];
        if (inString.hasNextInt()) {
            number = inString.nextInt();
            if (number > 0) {
                for (int i = 0; i < number; i++) {
                    teaKettle[i] = TeaKettle.createTeaKettle();
                }
            } else {
                System.out.println("The number must be greater than 0! ");
            }
        } else {
            System.out.print("These incorrect!");
        }
        printArray(teaKettle, number);
        TeaKettle[] newArray = sort(teaKettle, number);
        System.out.println();
        printArray(newArray, number);
    }

    public static void swap(TeaKettle[] array, int i, int j) {
        TeaKettle temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static TeaKettle[] sort(TeaKettle[] array, int number) {
        TeaKettle[] newArray = array.clone();
        for (int i = 0; i < number; i++) {
            for (int j = number-1; j > i; j--) {
                int result = newArray[j].compareTo(newArray[i]);
                if (result != 0) {
                    if (result < 0) {
                        swap(newArray, i, j);
                    }
                }
            }
        }
        return newArray;
    }

    public static void printArray(TeaKettle[] array, int number) {
        for (int i = 0; i < number; i++) {
            TeaKettle temp = array[i];
            System.out.println(temp.getCollor() + " " + temp.getVolume() + " " + temp.getType());
        }
    }

}