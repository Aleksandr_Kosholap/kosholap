<%--
  Created by IntelliJ IDEA.
  User: helldes
  Date: 15.02.2015
  Time: 19:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<form action="${pageContext.request.contextPath}/update-user" method="post">
  <input name="id" value="${user.id}">
  <input name="firstName" value="${user.firstName}">
  <input name="lastName" value="${user.lastName}">
  <input name="email" value="${user.email}" type="email">
  <select name="groupName" style="color: gray">
    <option style="color: gray" >${user.group.name}</option>
    <c:forEach var="group" items="${groups}">
      <option value=${group.id}>${group.name} </option>
    </c:forEach>
  </select>
  <input type="submit" value="Submit">
</form>
</body>
</html>
