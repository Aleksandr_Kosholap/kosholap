package ua.ck.geekhub;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.entity.Group;

@Controller
public class HelloController {

    @Autowired
    UserService userService;
    @Autowired
    GroupService groupService;

    public String error = "";


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {
        //	model.addAttribute("message", "Hello world!");
        return "redirect:users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String user(ModelMap model) {
        model.addAttribute("users", userService.getUsers());
        model.addAttribute("groups", groupService.getGroups());
        model.addAttribute("error", error);
        error = "";
        return "hello";
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public String createUser(
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam int groupName
    ) {
        try {
            userService.createUser(firstName, lastName, email, groupName);
        } catch (HibernateException e) {
            error = "The user has already been added to this Email";
        }
        return "redirect:users";
    }

    @RequestMapping(value = "/delete-user", method = RequestMethod.POST)
    public String deleteUser(
            @RequestParam int id
    ) {
        userService.deleteUser(userService.getUser(id));
        return "redirect:users";
    }

    @RequestMapping(value = "/delete-group", method = RequestMethod.POST)
    public String deleteGroup(
            @RequestParam int id
    ) {
        groupService.deleteGroup(groupService.getGroup(id));
        // userService.getUser(12);
        //  userService.deleteUser(new User());
        //userService.getUserH(12)
        return "redirect:users";
    }

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public String createGroup(
            @RequestParam String groupName
    ) {
        try {
            groupService.createGroup(groupName);
        } catch (HibernateException e) {
            error = "The group with the same name has already been created";
        }
        return "redirect:users";
    }

    @RequestMapping(value = "/edit-group", method = RequestMethod.POST)
    public String editGroup(
            @RequestParam int id, ModelMap model
    ) {
        model.addAttribute("group", groupService.getGroup(id));
        return "editGroup";
    }

    @RequestMapping(value = "/update-group", method = RequestMethod.POST)
    public String updateGroup(
            @RequestParam String name,
            @RequestParam int id
    ) {
        try {
            groupService.updateGroup(id, name);
        } catch (HibernateException e) {
            error = "The user has already been added to this Email";
        }
        return "redirect:users";
    }

    @RequestMapping(value = "/edit-user", method = RequestMethod.POST)
    public String editGroupUser(
            @RequestParam int id, ModelMap model
    ) {
        model.addAttribute("user", userService.getUser(id));
        model.addAttribute("groups", groupService.getGroups());
        return "editUser";
    }

    @RequestMapping(value = "/update-user", method = RequestMethod.POST)
    public String updateUser(
            @RequestParam int id,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam int groupName
    ) {
        try {
            Group group = (Group) groupService.getGroup(groupName);

            userService.updateUser(id, firstName, lastName, email, group);
        } catch (HibernateException e) {
            error = "hack";
        }
        return "redirect:users";
    }
}
