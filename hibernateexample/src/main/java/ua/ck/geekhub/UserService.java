package ua.ck.geekhub;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author vladimirb
 * @since 3/11/14
 */
@Repository
@Transactional
public class UserService {

	@Autowired
	SessionFactory sessionFactory;

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	public User getUser(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public List<User> getUsers() {
		return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
	}

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

	public void createUser(String firstName, String lastName, String email, int groupName) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setGroup((Group) sessionFactory.getCurrentSession().get(Group.class, groupName));
		saveUser(user);
	}
/*
    public User getUserH(Integer userId) {
        String hql = "select user from User user where user.id = :userId";
        return  (User) sessionFactory.getCurrentSession().createQuery(hql)
                .setInteger("userId", userId).uniqueResult();
    }
*/
	public void updateUser(int id, String firstName, String lastName, String email, Group groupName){

		User user = (User) sessionFactory.getCurrentSession().get(User.class, id);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setGroup(groupName);
		saveUser(user);
/*
		String hql = "UPDATE User SET user.firstName = :firstName, user.lastName = :lastName, user.email = :email, " +
				"user.groupName = :groupName  where user.id = :userId";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("userId", userId);
		query.setParameter("firstName", firstName);
		query.setParameter("lastName", lastName);
		query.setParameter("email", email);
		query.setParameter("groupName", groupName);
		query.executeUpdate();
		*/
	}
}
