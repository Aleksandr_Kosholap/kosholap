<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>

<table border="1">
    <table width="100%" border="0">
        <tbody>
        <tr>
            <td width="65%">
                <h1>Create User</h1>

                <form action="${pageContext.request.contextPath}/users" method="post">
                    <input name="firstName" placeholder="First Name">
                    <input name="lastName" placeholder="Last Name">
                    <input name="email" type="email" placeholder="Email">
                    <select name="groupName" style="color: gray">
                        <option style="color: gray" value="" disabled selected>Select Group</option>
                        <c:forEach var="group" items="${groups}">
                            <option value=${group.id}>${group.name} </option>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Submit">
                </form>
            </td>
            <td width="46%">
                <h1>Create Group</h1>

                <form action="${pageContext.request.contextPath}/groups" method="post">
                    <input name="groupName" placeholder="Group Name">

                    <input type="submit" value="Submit">
                </form>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <h1>Users Table</h1>
                <table border="1" width="80%">
                    <tr>
                        <td>ID</td>
                        <td>FIRST NAME</td>
                        <td>LAST NAME</td>
                        <td>EMAIL</td>
                        <td>GROUP</td>
                        <td>ACTION</td>
                    </tr>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td>${user.id}</td>
                            <td>${user.firstName}</td>
                            <td>${user.lastName}</td>
                            <td>${user.email}</td>
                            <td>${user.group.name}</td>
                            <td nowrap>
                                <table border="0">
                                    <tbody>
                                    <tr>
                                        <form name="users" action="/edit-user" method="post">
                                            <td height="10">
                                                <button value="${user.id}" name="id">Edit</button>
                                            </td>
                                        </form>
                                        <form name="users" action="/delete-user" method="post">
                                            <td>
                                                <button value="${user.id}" name="id">Delete</button>
                                            </td>
                                        </form>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
            <td width="46%" valign="top">
                <h1>Groups Table</h1>
                <table border="1" width="30%">
                    <tr>
                        <td>ID</td>
                        <td>NAME</td>
                        <td>ACTION</td>
                    </tr>
                    <c:forEach var="group" items="${groups}">
                        <tr>
                            <td>${group.id}</td>
                            <td>${group.name}</td>
                            <td nowrap>
                                <table border="0">
                                    <tbody>
                                    <tr>
                                        <form name="users" action="/edit-group" method="post">
                                            <td height="10">
                                                <button value="${group.id}" name="id">Edit</button>
                                            </td>
                                        </form>
                                        <form name="groups" action="/delete-group" method="post">
                                            <td>
                                                <button value="${group.id}" name="id">Delete</button>
                                            </td>
                                        </form>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <c:if test="${error != ''}">
    <script language="JavaScript">
        alert("${error}");
    </script>
    </c:if>
</body>
</html>