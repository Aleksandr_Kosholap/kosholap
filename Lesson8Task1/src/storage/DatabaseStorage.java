package storage;

import objects.Cat;
import objects.Entity;
import objects.Ignore;
import java.util.Map.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Implementation of {@link storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase();
        //  System.out.println(sql);
        List<T> result = null;
        try (Statement statement = connection.createStatement()) {
            result = extractResult(clazz, statement.executeQuery(sql));
            //  System.out.println(result);
        }
        return result;
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        //implement me
        String sql = "DELETE FROM " + entity.getClass().getSimpleName().toLowerCase() + " WHERE id = " + entity.getId();
        int rowsAffected = 0;
        try (Statement statement = connection.createStatement()) {
            rowsAffected = statement.executeUpdate(sql);
        }
        if (rowsAffected != 0){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        if (entity.isNew()) {
            //implement me
            //need to define right SQL query to create object
            String tableName = entity.getClass().getSimpleName();
            String fieldsTable = data.keySet().toString();
            fieldsTable = fieldsTable.substring(1, fieldsTable.length() - 1);
            String valueFields = data.values().toString();
            valueFields = valueFields.substring(1, valueFields.length() - 1);

            sql = "INSERT INTO " + tableName.toLowerCase() + " (" + fieldsTable + ") VALUES (" + valueFields + ")";

        } else {
            //implement me
            Statement statement = connection.createStatement();
            String tableName = entity.getClass().getSimpleName();
            String value = data.entrySet().toString();
            value = value.substring(1, value.length()-1);

            sql = "UPDATE " + tableName.toLowerCase() + " SET " + value + " WHERE id = " + entity.getId();

        }
        //implement me, need to save/update object and update it with new id if it's a creation
        PreparedStatement ps = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(entity.getId() == null){
            if (rs != null && rs.next()) {
                int key = rs.getInt(1);
                entity.setId(key);
            }
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> map = new LinkedHashMap();
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                if (field.get(entity) instanceof String) {
                    String valueString = "\"" + field.get(entity) + "\"";
                    map.put(field.getName(), valueString);
                } else {
                    if(field.get(entity) instanceof Boolean){
                        if (field.get(entity).equals(true)){
                            map.put(field.getName(), 1);
                        }else{
                            map.put(field.getName(), 0);
                        }
                    }else {
                        map.put(field.getName(), field.get(entity));
                    }
                }
            }
        }
        return map;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        //implement me
        List<T> listResult = new LinkedList<>();
        while (resultset.next()) {
            T clazzResult = clazz.newInstance();
            Field[] fields = clazzResult.getClass().getDeclaredFields();
            for(Field field : fields){
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.setAccessible(true);
                    // System.out.println(field.getName() + resultset.getObject(field.getName()));
                    field.set(clazzResult, resultset.getObject(field.getName()));
                    field.setAccessible(false);
                }
            }
            if (clazzResult.getId() == null) {
                clazzResult.setId(resultset.getInt("id"));
            }
            listResult.add(clazzResult);
        }
        return listResult;
    }
}
