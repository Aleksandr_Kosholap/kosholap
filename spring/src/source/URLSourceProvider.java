package source;

/**
 * Created by helldes on 07.02.2015.
 */
        import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
@Component
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URL url = new URL(pathToSource);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        URL url = new URL(pathToSource);
        String fileContent = "";
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream()))){
            String str = null;
            while( (str = buffer.readLine() ) != null ){
                fileContent = fileContent +str;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return fileContent;
    }
}
