package source;

/**
 * Created by helldes on 07.02.2015.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
@Component
public class SourceLoader {
    @Autowired
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    @Autowired public FileSourceProvider fileSourceProvider;
    @Autowired public URLSourceProvider urlSourceProvider;

    public SourceLoader() {
        //TODO: initialize me
        sourceProviders.add(fileSourceProvider);
        sourceProviders.add(urlSourceProvider);
    }

    public String loadSource(String pathToSource) throws IOException, MyException {
        //TODO: implement me
        String content = "";
        for (SourceProvider sp : sourceProviders) {
            if (sp.isAllowed(pathToSource)) {
                content = sp.load(pathToSource);
                if (content.equals("")) {
                    throw new MyException("No Content");
                }
                return content;
            }
        }
        return content;
    }
}
