package source;

/**
 * Created by helldes on 07.02.2015.
 */

import org.springframework.stereotype.Component;
import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
@Component
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File myFile = new File(pathToSource);
        return myFile.canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuilder fileContent = new StringBuilder();
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader( new FileInputStream(pathToSource)))){
            String str = "";
            while( (str = buffer.readLine() ) != null ){
                fileContent.append(str);
            }
        }catch (IOException e){

        }
        return fileContent.toString();
    }
}
