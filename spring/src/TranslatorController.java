/**
 * Created by helldes on 07.02.2015.
 */

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import source.MyException;
import source.SourceLoader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
@Configuration
@ComponentScan
public class TranslatorController {

    public static void main(String[] args) throws IOException {
        //initialization
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        SourceLoader sourceLoader = context.getBean(SourceLoader.class);
        Translator translator = context.getBean(Translator.class);

        System.out.println("Enter 'exit' to exit the program");
        System.out.println("Enter the path to the file :");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //TODO: add exception handling here to let user know about it and ask him to enter another path to translation
            //      So, the only way to stop the application is to do that manually or type "exit"
            String source = null;
            try {
                source = sourceLoader.loadSource(command);

                File fileSource = new File("Source.txt");
                FileWriter fileWriterSource = new FileWriter(fileSource);
                fileWriterSource.append(source);
                fileWriterSource.flush();
                fileWriterSource.close();

                String translation = translator.translate(source);

                File fileTranslate = new File("Translation.txt");
                FileWriter fileWriterTranslate = new FileWriter(fileTranslate);
                fileWriterTranslate.append(translation);
                fileWriterTranslate.flush();
                fileWriterTranslate.close();
                System.out.println("Original: " + source);
                System.out.println();
                System.out.println("Translation: " + translation);
            } catch (MyException e) {
                System.err.println(e);
            }
            System.out.println("Enter the path to the file :");
            command = scanner.next();
        }
    }
}

