/**
 * Created by helldes on 07.02.2015.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import source.URLSourceProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */
@Component
public class Translator {
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "trnsl.1.1.20141119T214335Z.b0053aa88bbbfb40.805868cef882b0765428a04e30326e3143ea1b49";
    private static final String TRANSLATION_DIRECTION = "uk";
    @Autowired
    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws IOException
     */
    public String translate(String original) throws IOException {
        //TODO: implement me
        String stringXML = new String();

        URL url = new URL(prepareURL(original));
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(url.openStream()))){
            String str = null;
            while( (str = buffer.readLine() ) != null ){
                stringXML = stringXML +str;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        String stringTranslate = parseContent(stringXML);
        return stringTranslate;
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text encodeText(text)
     */
    private String prepareURL(String text) {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) {
        //TODO: implement me
        int start = content.indexOf("<text>")+ 6;
        int end = content.lastIndexOf("</text>");
        String endString= content.substring(start, end);
        //TODO: implement me
        return endString;
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    public String encodeText(String text) {
        String url = URLEncoder.encode(text);
        return url;
    }
}
