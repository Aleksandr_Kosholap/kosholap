import java.util.Scanner;

public class factorial {

        public static void main(String[] args) {
            System.out.println("Введите n: ");
            Scanner inString = new Scanner(System.in);
            if (inString.hasNextInt()) {
                int result = 1;
                int number = inString.nextInt();
                if (number >= 0) {
                    for (int i = 1; i <= number; i++) {
                        result = result * i;
                    }
                } else {
                    System.out.print("Число должно быть больше 0!");
                    return;
                }
                System.out.println("Факториал " + number + " равен " + result);
            } else {
                System.out.print("Данные не коректные!");
            }
        }
    }

